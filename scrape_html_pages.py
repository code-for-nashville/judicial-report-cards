import pathlib

from datetime import datetime, timedelta
from requests_html import HTMLSession, HTML

from get_logging import get_logger
from input_dates import input_dates

""" Defines a method that will request the Detailed Criminal History pages
    of all defendants appearing before the Criminal Court within a chosen timeframe and save those
    html documents to the /output directory in a unique subdir
"""


def scrape(starting_date: datetime, ending_date: datetime):

    start_date_string = starting_date.strftime('%m-%d-%Y')
    ending_date_string = ending_date.strftime('%m-%d-%Y')

    # create a directory for this scrape based on it
    output_dir = pathlib.Path(f'html_output/{start_date_string}_{ending_date_string}')
    output_dir.mkdir(exist_ok=True)

    # set up logging
    log_file_name = f"{output_dir}/log_{start_date_string}_{ending_date_string}"

    log_format = '%(asctime)s:%(message)s'

    logger = get_logger(__name__, log_file_name, log_format=log_format)
    logger.info(f'STARTING RUN: {datetime.now().strftime("%m/%d/%Y %I:%M:%S")}')

    # set up scraping
    base_url = 'https://sci.ccc.nashville.gov'
    session = HTMLSession()

    # Scrape_range is a timedelta instance with a 'days' attribute used as the range incrementer
    scrape_range = starting_date - ending_date

    current_scrape_date = starting_date

    try:
        for day in range(scrape_range.days):
            logger.info(f'scraping {current_scrape_date.strftime("%m/%d/%Y")}')

            identified_defendants = []

            scrape_date_string = current_scrape_date.strftime('%m/%d/%Y')
            search_page_response = session.post(f'{base_url}/Reporting/TrialCourtScheduledAppearance',
                                                data={'reportDate': scrape_date_string})

            defendant_links = search_page_response.html.find("[data-content='View Defendant Details']")

            for link in defendant_links:
                url_sections = get_url_sections(link)
                name_identifier = url_sections[3]
                if name_identifier not in identified_defendants:
                    defendant_details_page_url = f'{base_url}/Search/CriminalHistory?P_CASE_IDENTIFIER={name_identifier}'
                    name_page_response = session.get(defendant_details_page_url)
                    name_page_html = HTML(html=name_page_response.text)
                    with open(f"{output_dir}/{name_identifier}.html", "w") as outfile:
                        outfile.write(name_page_html.html)
                    identified_defendants.append(name_identifier)

            current_scrape_date = current_scrape_date - timedelta(days=1)

        logger.info("Run completed successfully!")

    except Exception as e:
        logger.exception(f"unable to complete scrape due to {str(e)}")

    logger.info(f'RUN ENDED: {datetime.now().strftime("%m/%d/%Y %I:%M:%S%p")}')

def get_url_sections(link):
    return link.attrs['href'].split('/')


# Execution
if __name__ == '__main__':
    start, end = input_dates()
    scrape(start, end)
