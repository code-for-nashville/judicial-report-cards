from requests_html import HTML
import pandas as pd
from constants import FIELDS_WITH_XPATHS
from pathlib import Path
from datetime import datetime

from get_logging import get_logger

""" Iterate through all sub-directories in html_output, convert the html pages found in them to dataframes, and collect
    all those dataframes into a single CSV
 """

# TODO: Make this function able to receive a specific directory or list/range thereof


def merge_html():

    all_dirs = Path('html_output').iterdir()
    all_dirs = [x for x in all_dirs if x.is_dir()]

    output_dir = get_output_dir(all_dirs)

    log_file_name = f"{output_dir}/log"
    logger = get_logger(__name__, log_file_name, log_format='%(asctime)s:%(message)s')

    all_dfs = []

    for subdir in all_dirs:
        subdir_path = Path(subdir)

        all_html_files = subdir_path.glob('*.html')

        for file in all_html_files:
            defendant_df = pd.DataFrame(columns=FIELDS_WITH_XPATHS.keys())

            logger.info(f"merging {file}")
            with open(file) as infile:

                source_code = infile.read()
                case_html = HTML(html=source_code)
                case_sections = case_html.find(".crim-history-row")

                for i in range(len(case_sections)):
                    row = []
                    for xpath in FIELDS_WITH_XPATHS.values():
                        element = case_sections[i].xpath(xpath, first=True)
                        value = ''
                        if element:
                            value = element.text
                        row.append(value)
                    defendant_df.loc[i] = row

            all_dfs.append(defendant_df)

    merged = pd.concat(all_dfs).drop_duplicates()

    with open(f'{output_dir}/all_results_{output_dir.name}.csv', 'w+') as outfile:
        merged.to_csv(outfile, index=False)

    logger.info(f"merge complete!")


def get_output_dir(all_dirs: list) -> Path:
    """ Helper to determine what we should name the output dir to hold the results

    This is based on finding the earliest and latest dates in the entire set of scrape runs
    that we're merging

    """
    dir_name_strings = [x.name for x in all_dirs]
    dir_dates = []

    for dir_name_string in dir_name_strings:
        dir_dates.extend(dir_name_string.split("_"))

    dir_dates = [datetime.strptime(x, '%m-%d-%Y') for x in dir_dates]

    earliest = min(dir_dates).strftime('%m-%d-%Y')
    latest = max(dir_dates).strftime('%m-%d-%Y')

    output_dir = Path(f"csv_output/{earliest}_{latest}")
    output_dir.mkdir(exist_ok=True)

    return output_dir


if __name__ == "__main__":
    merge_html()

