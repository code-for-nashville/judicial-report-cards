import logging


def get_logger(logger_name, file_name, level=logging.DEBUG, log_format='') -> logging.Logger:
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)

    formatter = logging.Formatter(log_format)

    handler = logging.FileHandler(file_name)
    stream = logging.StreamHandler()

    handler.setFormatter(formatter)
    stream.setFormatter(formatter)

    logger.addHandler(handler)
    logger.addHandler(stream)

    return logger
